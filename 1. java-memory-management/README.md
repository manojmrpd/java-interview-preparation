# Java Memory Management
## List of the Questions
- What is the Difference between Stack and Heap Allocation?
- What is the Difference between JDK, JRE and JVM?
- What are Class Loader and its Types?
- How JVM works in Java?
- Explain JVM Memory Management?
- What are Key features of Stack Memory and Heap Memory in JVM?
- How Garbage Collectors works in Java?
- How to make object eligible for garbage collection in Java?
- How to prevent objects of a class from Garbage Collection in Java?
- Explain Mark and Sweep Garbage Collection Algorithm?
- What are Memory Leaks in java?
- Explain OutOfMemoryError Exception in java?
- Explain Java (JVM) Memory Model?
